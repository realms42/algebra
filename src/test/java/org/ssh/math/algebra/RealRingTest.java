package org.ssh.math.algebra;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * The Test RealRingTest.
 *
 * This class tests the implementation of a {@link Ring} formed from the real numbers and the traditional operations
 * of addition and multiplication.
 *
 * @author Rimon Oz
 */
public class RealRingTest {
    private RealRing realRing = new RealRing();
    private final Double firstValue = 3d;
    private final Double secondValue = -2d;
    private final Double thirdValue = 2d;
    private final Double zero = 0d;
    private final Double one = 1d;


    /**
     * Tests the functionality of the {@link RealRing#add} method.
     *
     * @throws Exception Thrown when the test fails.
     */
    @Test
    public void add() throws Exception {
        //  *   - Closure:        if a and b are elements of F, then a + b is an element of F
        // proven by type system
        //  *   - Associativity:  if a, b, and c are elements of F, then (a+b)+c = a+(b+c)
        assertEquals(this.realRing.add(this.realRing.add(this.firstValue, this.secondValue), this.thirdValue),
                this.realRing.add(this.firstValue, this.realRing.add(this.secondValue, this.thirdValue)));
        //  *   - Identity:       there exists an element e of F such that for all a in F: e+a = a+e = a+0
        assertEquals(this.realRing.add(this.zero, this.firstValue),
                this.realRing.add(this.firstValue, this.zero));
        //  *   - Distributivity: if a, b, and c are elements of F, then a*(b+c) = a*b + a*c
        assertEquals(this.realRing.multiply(this.firstValue, this.realRing.add(this.secondValue, this.thirdValue)),
                this.realRing.add(
                        this.realRing.multiply(this.firstValue, this.secondValue),
                        this.realRing.multiply(this.firstValue, this.thirdValue)));
    }

    /**
     * Tests the functionality of the {@link RealRing#multiply} method.
     *
     * @throws Exception Thrown when the test fails.
     */
    @Test
    public void multiply() throws Exception {
        //  *   - Closure:        if a and b are elements of F, then a + b is an element of F
        // proven by type system
        //  *   - Associativity:  if a, b, and c are elements of F, then (a*b)*c = a*(b*c)
        assertEquals(this.realRing.multiply(this.realRing.multiply(this.firstValue, this.secondValue), this.thirdValue),
                this.realRing.multiply(this.firstValue, this.realRing.multiply(this.secondValue, this.thirdValue)));
        //  *   - Inverse:        for every element, a, in F there exists another element, b, such that a*b=1
        assertEquals(this.realRing.multiply(this.firstValue, 1d/this.firstValue), this.one);
        // a few simple checks
        // assertEquals(this.realRing.multiply(this.secondValue, this.zero), this.zero);  <-- in Java -0.0d != 0.0d
        // so the above test doesn't work!
        assertTrue(this.realRing.multiply(this.secondValue, this.zero) == 0d
                || this.realRing.multiply(this.secondValue, this.zero) == -0d);
        assertEquals(this.realRing.add(
                this.realRing.multiply(this.firstValue, this.secondValue),
                this.realRing.multiply(this.firstValue, this.thirdValue)),
                this.zero);
    }

    /**
     * Tests the functionality of the {@link RealRing#divide} method.
     *
     * @throws Exception Thrown when the test fails.
     */
    @Test
    public void divide() throws Exception {
        // a few simple checks
        assertEquals(this.realRing.divide(this.firstValue, this.one), this.firstValue);
        // division by zero
        try {
            this.realRing.divide(this.firstValue, this.zero);
        }
        catch (Exception exception) {
            assertTrue(exception instanceof ArithmeticException);
        }
    }

    /**
     * Tests the functionality of the {@link RealRing#getAdditiveInverse} method.
     *
     * @throws Exception Thrown when the test fails.
     */
    @Test
    public void getAdditiveInverse() throws Exception {
        // zero is the only number whose additive inverse is itself
        // assertEquals(this.realRing.getAdditiveInverse(this.zero), this.zero); <-- in Java -0.0d != 0.0d
        // so the above test doesn't work!
        assertTrue(this.realRing.getAdditiveInverse(this.zero) == 0d
                || this.realRing.getAdditiveInverse(this.zero) == -0d);
        // check if its truly an additive inverse
        assertEquals(this.realRing.add(this.realRing.getAdditiveInverse(this.firstValue), this.firstValue), this.zero);
    }

    /**
     * Tests the functionality of the {@link RealRing#getMultiplicativeInverse} method.
     *
     * @throws Exception Thrown when the test fails.
     */
    @Test
    public void getMultiplicativeInverse() throws Exception {
        // one is the only number whose multiplicative inverse is itself
        assertEquals(this.realRing.getMultiplicativeInverse(this.one), this.one);
        // check if its truly a multiplicative inverse
        assertEquals(this.realRing.multiply(this.realRing.getMultiplicativeInverse(this.firstValue), this.firstValue),
                this.one);
    }

}