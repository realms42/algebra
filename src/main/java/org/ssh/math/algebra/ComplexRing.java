package org.ssh.math.algebra;

/**
 * @author Emre Elmas
 */
public class ComplexRing implements Ring<ComplexNumber>{
    /**
     *
     * @param   firstElement  The first field element to be added.
     * @param   secondElement The second field element to be added.
     * @return
     */
    @Override
    public ComplexNumber add(ComplexNumber firstElement, ComplexNumber secondElement) {
        return ComplexNumber.add(firstElement, secondElement);
    }
    /**
     *
     * @param  fieldElement The number to compute the additive inverse for.
     * @return
     */
    @Override
    public ComplexNumber getAdditiveInverse(ComplexNumber fieldElement) {
        ComplexNumber x = new ComplexNumber(fieldElement);
        x.setReal(x.getReal()*(-1d));
        x.setComplex(x.getComplex()*(-1d));
        return x;
    }
    /**
     *
     * @param   firstElement  The first field element to be multiplied.
     * @param   secondElement The second field element to be multiplied.
     * @return
     */
    @Override
    public ComplexNumber multiply(ComplexNumber firstElement, ComplexNumber secondElement) {
        return ComplexNumber.multiply(firstElement, secondElement);
    }
    /**     A / (A^2+B^2) - B / (A^2+B^2) where A is the real number and B is the complex number
     *
     * @param  fieldElement The complex number to compute the multiplicative inverse for.
     * @return
     * @throws Exception
     */
    @Override
    public ComplexNumber getMultiplicativeInverse(ComplexNumber fieldElement) throws Exception {
        if (fieldElement.getReal() == 0 && fieldElement.getComplex() == 0)  //division by 0
            throw new ArithmeticException();
        ComplexNumber x = new ComplexNumber(fieldElement);
        double scale = Math.pow(x.getReal(), 2d) + Math.pow(x.getComplex(), 2d);
        x.setReal(x.getReal() / scale);
        x.setComplex( -x.getComplex() / scale);
        return x;
    }

}
