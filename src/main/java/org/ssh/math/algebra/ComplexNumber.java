package org.ssh.math.algebra;

/**
 * @author Emre Elmas
 */
public class ComplexNumber{
    private double real;
    private double complex;

    public ComplexNumber(double x, double y){
        real = x;
        complex = y;
    }

    public void setReal(double x){
        real = x;
    }
    public void setComplex(double x){
        complex = x;
    }

    public double getReal(){
        return real;
    }
    public double getComplex(){
        return complex;
    }

    /** Copy constructor
     *
     * @param aComplexNumber
     */
    public ComplexNumber(ComplexNumber aComplexNumber){
        this(aComplexNumber.getReal(), aComplexNumber.getComplex());
    }

    /**
     * @param x
     * @param y
     * @return x + y
     */
    public static ComplexNumber add(ComplexNumber x, ComplexNumber y){
        return new ComplexNumber(
                x.getReal() + y.getReal(),
                x.getComplex() + y.getComplex());
    }

    /**
     *
     * @param x
     * @param y
     * @return x*y
     */
    public static ComplexNumber multiply(ComplexNumber x, ComplexNumber y){
        return new ComplexNumber(
                x.getReal() * y.getReal() - x.getComplex() * y.getComplex(),
                x.getReal() * y.getComplex() + x.getComplex() * y.getReal());
    }
}
