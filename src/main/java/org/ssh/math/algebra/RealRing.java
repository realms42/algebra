package org.ssh.math.algebra;

/**
 * The Class
 */
public class RealRing implements Ring<Double> {
    @Override
    public Double add(Double firstElement, Double secondElement) {
        return firstElement + secondElement;
    }

    @Override
    public Double getAdditiveInverse(Double fieldElement) {
        return (-1d)*fieldElement;
    }

    @Override
    public Double multiply(Double firstElement, Double secondElement) {
        return firstElement * secondElement;
    }

    /**
     * @param fieldElement The number to compute the multiplicative inverse for.
     * @return
     * @throws Exception
     */
    @Override
    public Double getMultiplicativeInverse(Double fieldElement) throws Exception {
        if (fieldElement == 0)
            throw new ArithmeticException();
        return 1d/fieldElement;
    }
}
