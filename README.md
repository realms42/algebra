# algebra

In this tutorial we will be looking at two of the most fundamental data structures; rings and groups.
These data structures are so common that you have used them often, probably without knowing. These data 
structures (like most things in computer science) are mathematical objects which we use to formally define
operations, such as addition and multiplication, for a set of numbers (or other objects!).

The point of this exercise is not the mathematics, even though it's
[very](http://math.stackexchange.com/questions/449066/do-groups-rings-and-fields-have-practical-applications-in-cs-if-so-what-are-s)
[interesting](http://math.stackexchange.com/a/449076), but the testing! The goal of this exercise
is to create a `ComplexRing` which is an implementation of `Ring<F>` (just as `RealRing` is).
 
In order to create a `ComplexRing` you must first create a `ComplexNumber` which has the following (suggested)
interface:

  - `ComplexNumber add(ComplexNumber)`
  - `ComplexNumber subtract(ComplexNumber)`
  - `ComplexNumber multiply(ComplexNumber)`
  - `ComplexNumber divide(ComplexNumber)`
  
> Don't forget the javadoc! IntelliJ generates a large amount of the javadoc for you, so if it's missing
from the code then you are actively working against your IDE ;).
  
And then create a `ComplexRing implements Ring<ComplexNumber>`.

The test suite can be similar to the test suite found in `RealRingTest`, but whatever you do make sure to test all
properties and axioms! 

You should host your version of the project on your own GitLab-page. You can quickly make a copy of this project
by clicking the **Fork**-button at the top of the project page.

If you're confused about the assignment then let me offer you some 
[words](http://www.javaworld.com/article/2078556/mobile-java/10-hard-truths-developers-must-learn-to-accept.html) of 
[wisdom](https://twitter.com/CodeWisdom):

  1. Programming is as hard as its rewarding; isn't that why we like it so much?
  1. The field of computer science, and the business of software, changes every day. This means that you will
  most likely end up working with (or trying out) new development tools regularly.
  1. Since programming is the art of automation you will potentially end up working on automation projects from
  a broad range of fields. Your general knowledge is therefore your power.
  1. If you familiarize yourself with situations in which you are handed a nasty problem, then you
  will become better at solving them. The key is, how cruel it may sound, to plow through.
